<?php

namespace app\services;
use app\models\Transaction;
use yii\base\ErrorException;

/**
 * Class Transaction
 * @package app\services
 */
class TransactionService
{
    /**
     * @param Transaction $transaction
     * @return bool
     * @throws ErrorException
     */
    public function execute(Transaction $transaction)
    {
        $this->ensureTransactionAvailable($transaction);

        $transaction->sender->balance -= $transaction->amount;
        $transaction->recipient->balance += $transaction->amount;

        return $transaction->sender->save()
            && $transaction->recipient->save()
            && $transaction->save();
    }

    /**
     * @param Transaction $transaction
     * @throws ErrorException
     */
    public function ensureTransactionAvailable(Transaction $transaction)
    {
        $transaction->validate();

        switch (true) {
            case !$transaction->isNewRecord:
                throw new ErrorException('Transaction has already made');
            case $transaction->hasErrors():
                throw new ErrorException(implode('; ', $transaction->getFirstErrors()));
            case $transaction->sender->balance < $transaction->amount:
                throw new ErrorException('Sender doesn\'t have enough money');
        }
    }
}