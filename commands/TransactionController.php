<?php

namespace app\commands;

use app\models\Transaction;
use app\models\User;
use app\services\TransactionService;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\console\widgets\Table;

/**
 * Class TransactionController
 * @package commands
 */
class TransactionController extends Controller
{
    /**
     * @var int sender user Id
     */
    public $senderId;

    /**
     * @var int recipient user Id
     */
    public $recipientId;

    /**
     * @var float Transaction amount
     */
    public $amount;

    /**
     * @param string $actionID
     * @return string[]
     */
    public function options($actionID): array
    {
        switch ($actionID) {
            case 'execute':
                return ['senderId', 'recipientId', 'amount'];
            default:
                return [];
        }
    }

    /**
     * @return int
     * @throws \yii\db\Exception
     */
    public function actionExecute(): int
    {
        $dbTransaction = \Yii::$app->db->beginTransaction();

        try {
            $this->lockUsers(['id' => [$this->senderId, $this->recipientId]]);
            $moneyTransaction = new Transaction([
                'sender_id' => $this->senderId,
                'recipient_id' => $this->recipientId,
                'amount' => round($this->amount, 2, PHP_ROUND_HALF_DOWN)
            ]);
            $transactionService = new TransactionService();

            if (!$transactionService->execute($moneyTransaction)) {
                throw new ErrorException('Transaction failed');
            };

            $dbTransaction->commit();
        } catch (\Exception $e) {
            $dbTransaction->rollBack();
            echo 'Error: ' . $e->getMessage() . PHP_EOL;

            return ExitCode::DATAERR;
        }

        echo 'Transaction completed' . PHP_EOL;

        return ExitCode::OK;
    }

    /**
     * @param int $limit
     * @return int
     * @throws \Exception
     */
    public function actionList($limit = 20): int
    {
        $transactions = Transaction::find()
            ->select([
                'created_at',
                'sender' => 'sender.name',
                'recipient' => 'recipient.name',
                'amount'
            ])
            ->joinWith(['sender', 'recipient'], false)
            ->orderBy(['created_at' => SORT_DESC])
            ->limit($limit)
            ->asArray()
            ->all();

        echo Table::widget([
            'headers' => ['Время', 'Отправитель', 'Получатель', 'Сумма'],
            'rows' => $transactions
        ]) . PHP_EOL;

        return ExitCode::OK;
    }

    /**
     * @param array $condition
     */
    protected function lockUsers(array $condition): void
    {
        User::findBySql( //because Yii2 Query Builder doesn't support FOR UPDATE statement
            User::find()
                ->andWhere($condition)
                ->createCommand()
                ->getRawSql()
            . ' FOR UPDATE'
        )->all();
    }
}