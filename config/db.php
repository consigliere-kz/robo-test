<?php
$host = getenv('DB_HOST') ?: 'localhost';
$port = getenv('DB_PORT') ?: 3306;
$dbname = getenv('DB_NAME');

return [
    'class' => \yii\db\Connection::class,
    'dsn' => sprintf('mysql:host=%s;port=%d;dbname=%s', $host, $port, $dbname),
    'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
    'enableSchemaCache' => true
];