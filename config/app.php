<?php

$config = [
    'id' => 'test-app',
    'basePath' => __DIR__ . '/../',
    'controllerNamespace' => 'app\commands',
    'components' => [
        'db' => require "db.php"
    ],
    'aliases' => [
        '@app' => __DIR__ . '/../',
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;